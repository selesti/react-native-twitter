import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native'
import {Icon} from "expo";

export default class TweetScreen extends React.Component {
    static navigationOptions = {
        title: 'Tweet',
    };

    constructor(props) {
        super(props);

        this.tweet = this.props.navigation.getParam('activeTweet');
    }

    render() {

        const tweet = this.tweet;

        const comment = comment => {
            return (
                <View key={ comment.key } style={{ padding: 20 }}>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View>
                            <Image style={ styles.avatar } source={{ uri: comment.user.avatar }} />
                        </View>

                        <View style={{ marginTop: 6, marginLeft: 20 }}>
                            <Text style={{ fontWeight: 'bold' }}>{ comment.user.name }</Text>
                            <Text>{ comment.user.handle }</Text>
                        </View>
                    </View>

                    <Text style={{ marginLeft: 70, marginTop: 10, fontSize: 18, lineHeight: 24 }}>{ comment.body }</Text>


                </View>
            )
        };

        return (
            <ScrollView>
                <View style={ styles.row }>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View>
                            <Image style={ styles.avatar} source={{ uri: tweet.user.avatar }} />
                        </View>
                        <View>
                            <View style={{ left: 20, top: 5 }}>
                                <Text style={{ fontWeight: 'bold' }}>{ tweet.user.name }</Text>
                                <Text>{ tweet.user.handle }</Text>
                            </View>
                        </View>
                    </View>

                    <Text style={{ marginTop: 10, fontSize: 26, lineHeight: 37 }}>{ tweet.body }</Text>

                    <View style={ styles.actions }>
                        <View style={{ flex: 0, flexDirection: 'row' }}>
                            <Icon.Ionicons name="ios-chatbubbles" size={20} />
                            { tweet.comments.length > 0 && <Text style={{ marginLeft: 7, top: 2 }}>{ tweet.comments.length }</Text> }
                        </View>

                        <View style={{ flex: 0, flexDirection: 'row', marginLeft: 30 }}>
                            <Icon.Ionicons name="ios-heart" size={20} />
                            { tweet.likes > 0 && <Text style={{ marginLeft: 7, top: 2 }}>{ tweet.likes }</Text> }
                        </View>
                    </View>

                </View>

                <View>
                    { tweet.comments.map(item => comment(item)) }
                </View>

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    row: {
        flex: 1,
        // flexDirection: 'row',
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
    },
    tweet: {
        paddingLeft: 20,
        flex: 1,
    },
    actions: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 10,
    }
});
