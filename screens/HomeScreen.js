import Api from '../Api'
import React from 'react';
import { Modal, StyleSheet, Text, View, FlatList, Image, TouchableHighlight, SafeAreaView } from 'react-native';
import { Icon } from 'expo'

export default class App extends React.Component {

    static navigationOptions = {
        title: 'My Feed',
    };

    constructor(props) {
        super(props);

        this.api = new Api;

        this.state = {
            refreshing: false,
            tweets: this.api.refresh(),
            activeTweet: null
        };

        // this.openTweet(this.state.tweets[0])
    }

    loadMore() {
        this.setState({
            tweets: this.api.loadMore()
        })
    }

    refresh() {
        this.setState({
            tweets: this.api.refresh()
        })
    }

    openTweet(activeTweet) {
        this.props.navigation.navigate('Tweet', { activeTweet })
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    style={styles.list}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.refresh()}
                    onEndReached={() => this.loadMore()}
                    data={this.state.tweets}
                    renderItem={({item}) => <Tweet onPress={() => this.openTweet(item) } tweet={ item } />}
                />
            </View>
        )
    }
}

class Tweet extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const tweet = this.props.tweet;

        return (
            <TouchableHighlight onPress={ this.props.onPress }>
                <View style={ styles.row }>
                    <View>
                        <Image style={ styles.avatar} source={{ uri: tweet.user.avatar }} />
                    </View>

                    <View style={ styles.tweet }>
                        <View style={ styles.meta }>
                            <Text style={{ fontWeight: 'bold' }}>{ tweet.user.name }</Text>
                            <Text style={{ paddingLeft: 10 }}>{ tweet.user.handle }</Text>
                        </View>

                        <Text style={{ marginTop: 10 }}>{ tweet.body }</Text>

                        <View style={ styles.actions }>
                            <View style={{ flex: 0, flexDirection: 'row' }}>
                                <Icon.Ionicons name="ios-chatbubbles" size={20} />
                                { tweet.comments.length > 0 && <Text style={{ marginLeft: 7, top: 2 }}>{ tweet.comments.length }</Text> }
                            </View>

                            <View style={{ flex: 0, flexDirection: 'row', marginLeft: 30 }}>
                                <Icon.Ionicons name="ios-heart" size={20} />
                                { tweet.likes > 0 && <Text style={{ marginLeft: 7, top: 2 }}>{ tweet.likes }</Text> }
                            </View>
                        </View>

                    </View>
                </View>

            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
    list: {
        height: '100%'
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
    },
    tweet: {
        paddingLeft: 20,
        flex: 1,
    },
    meta: {
        flex: 1,
        flexDirection: 'row'
    },
    actions: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 10,
    }
});
