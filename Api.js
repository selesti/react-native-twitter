import faker from 'faker';

export default class Api
{
    data = [];

    _generateTweets(number = null) {
        number = number || faker.random.number({ min: 1, max: 5 });

        const items = [];
        let n = 0;

        while (n < number) {
            items.push(this._generateTweet())
            n++
        }

        return items;
    }

    _generateTweet() {

        const comments = new Array(faker.random.number({ min: 0, max: 10 })).fill(true).map(() => {
            return {
                user: {
                    name: faker.name.findName(),
                    handle: '@' + faker.internet.userName(),
                    avatar: faker.internet.avatar()
                },
                key: faker.random.uuid(),
                body: faker.lorem.paragraph(),
            }
        });

        return {
            user: {
                name: faker.name.findName(),
                handle: '@' + faker.internet.userName(),
                avatar: faker.internet.avatar()
            },
            key: faker.random.uuid(),
            body: faker.lorem.paragraph(),
            likes: faker.random.number({ min: 0, max: 200 }),
            comments,
        }
    }

    refresh() {
        const startCount = this.data.length ? null : 10;

        const newTweets = this._generateTweets(startCount);

        this.data = newTweets.concat(this.data);

        return this.data;
    }

    loadMore() {
        const oldTweets = this._generateTweets();

        this.data = this.data.concat(oldTweets);

        return this.data;
    }
}
