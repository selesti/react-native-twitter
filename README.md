# Tasks

## Setup page and tab titles along with the tab icon
- MainTabNavigator
- logo-twitter
- navigationOptions

## Install API class
- npm i faker
- https://gist.github.com/OwenMelbz/8730a7557a2f37cd516daecf45dfb28d

## Display feed of tweets
- Avatar
- Meta
- Message
- likes/comments count

## Display individual tweet
- New screen
- Display comments
- MainTabNavigator

## Pull to refresh
- State / refreshing
- Api.refresh()

## Scroll down to load more
- State / refreshing